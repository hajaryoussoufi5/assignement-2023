package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.dto.DepositDto;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
@Component
public class DepositMapper {
    private static DepositDto depositDto;


    public  static DepositDto map(Deposit deposit){

        depositDto = new DepositDto();
        depositDto.setNomPrenomEmetteur(deposit.getNomPrenomEmetteur());
        depositDto.setRib(deposit.getCompteBeneficiaire().getRib());
        depositDto.setMontant(deposit.getMontant());
        depositDto.setDate(deposit.getDateExecution());
        depositDto.setMotif(deposit.getMotifDeposit());

        return depositDto;
    }

    public List<DepositDto> loadAll(List<Deposit> list){
       List<DepositDto> depositDtoList = new ArrayList<>();
       for(Deposit depot : list)
           depositDtoList.add(map(depot));
            return depositDtoList;
    }


}
