package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransferDto;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
@Component
public class TransferMapper {

    private static TransferDto transferDto;

    public static TransferDto map(Transfer transfer) {
        transferDto = new TransferDto();
        transferDto.setNrCompteEmetteur(transfer.getCompteEmetteur().getNrCompte());
        transferDto.setDate(transfer.getDateExecution());
        transferDto.setMotif(transfer.getMotifTransfer());

        transferDto.setNrCompteBeneficiaire(transfer.getCompteBeneficiaire().getNrCompte());
        transferDto.setMontant(transfer.getMontantTransfer());

        return transferDto;

    }

    public List<TransferDto> loadAll(List<Transfer> list){
        List<TransferDto> transferDtoList= new ArrayList<>();
        for(Transfer transfer:list)
            transferDtoList.add(map(transfer));
        return transferDtoList;
    }
}
