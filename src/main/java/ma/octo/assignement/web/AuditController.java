package ma.octo.assignement.web;


import ma.octo.assignement.domain.Audit;
import ma.octo.assignement.service.AuditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/api/v1/audits")
public class AuditController {
    @Autowired
    private  AuditService auditService;

    @GetMapping
    List<Audit> loadAll(){
        return auditService.loadAllAudit();
    }

}
