package ma.octo.assignement.web;

import ma.octo.assignement.service.constantes.Constantes;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.TransferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/api/v1/transfers")
class TransferController {
    @Autowired
    private TransferService transferService;


    @GetMapping
    List<TransferDto> loadAll() {

        return transferService.loadAllTransfer();
    }


    @PostMapping ( path = "/transfer")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<String> createTransaction(@RequestBody TransferDto transferDto)
            throws TransactionException, CompteNonExistantException, SoldeDisponibleInsuffisantException {
        transferService.createTransaction(transferDto);
        return new ResponseEntity<>(Constantes.TRANSFER_CREER, HttpStatus.OK);
    }



}
