package ma.octo.assignement.web;

import ma.octo.assignement.service.constantes.Constantes;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.DepositService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping(path = "/api/v1/deposits")
public class DepositController {
    @Autowired
    private DepositService depositService;

    @GetMapping
    public List<DepositDto> loadAll(){
        return depositService.loadAllDeposit();
    }


    @PostMapping(path = "/deposit")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<String> createDeposit(@RequestBody DepositDto depositDto)
            throws TransactionException, CompteNonExistantException {
        depositService.createDeposit(depositDto);
        return new ResponseEntity<>(Constantes.DEPOT_CREER, HttpStatus.OK);
    }
}


