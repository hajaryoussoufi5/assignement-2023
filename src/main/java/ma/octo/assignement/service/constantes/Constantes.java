package ma.octo.assignement.service.constantes;

import java.math.BigDecimal;

public final class  Constantes {
    public static final BigDecimal MONTANT_MAXIMAL = new BigDecimal(10000);
    public static final BigDecimal MONTANT_MINIMAL = new BigDecimal(10);
    public static final String COMPTE_E_NON_EXISTANT = "Compte Emetteur Non existant";
    public static final String COMPTE_B_NON_EXISTANT = "Compte Beneficiere Non existant";
    public static final String MONTANT_VIDE = "Montant vide";
    public static final String MONTANT_OPERATION_MIN = "Montant minimal non atteint";
    public static final String MONTANT_OPERATION_MAX = "Montant maximal dépassé";
    public static final String MOTIF_VIDE = "Motif vide";
    public static final String SOLDE_INSUFFISANT = "Solde insuffisant pour l'utilisateur";
    public static final String DEPOT_CREER = "Le Depot est fait avec succes";
    public static final String TRANSFER_CREER = "Le Transfert est fait avec succes";

    public static final String USER_AUTH_NOT_FOUND = "Login introuvable";

    private Constantes(){}
}
