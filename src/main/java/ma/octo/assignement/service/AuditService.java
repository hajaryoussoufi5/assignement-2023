package ma.octo.assignement.service;

import ma.octo.assignement.domain.Audit;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.repository.AuditRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;

@Service
@Transactional
public class AuditService {

    Logger logger = LoggerFactory.getLogger(AuditService.class);

    @Autowired
    private AuditRepository auditRepository;


    public void auditTransfer(String message) {

        logger.info("Audit de l'événement {}", EventType.TRANSFER);

        Audit audit = new Audit();
        audit.setEventType(EventType.TRANSFER);
        audit.setMessage(message);

        auditRepository.save(audit);
    }


    public void auditDeposit(String message) {

        logger.info("Audit de l'événement {}", EventType.DEPOSIT);

        Audit audit = new Audit();
        audit.setEventType(EventType.DEPOSIT);
        audit.setMessage(message);
        auditRepository.save(audit);
    }

    public List<Audit> loadAllAudit() {
        logger.info("Lister des Audit");
        var all = auditRepository.findAll();

        if (CollectionUtils.isEmpty(all)) {
            return Collections.emptyList();
        } else {
            return  all;
        }
    }


}
