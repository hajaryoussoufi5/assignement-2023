package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.TransferMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.TransferRepository;
import ma.octo.assignement.service.constantes.Constantes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestBody;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

@Service
public class TransferService {
    Logger logger = LoggerFactory.getLogger(TransferService.class);
    @Autowired
    private TransferRepository transferRepository;
    @Autowired
    private  CompteRepository compteRepository;
    @Autowired
    private AuditService auditService;
    @Autowired
    private TransferMapper transferMapper;

    public List<TransferDto> loadAllTransfer() {
        logger.info("Lister des transfer");

        var all = transferRepository.findAll();

        if (CollectionUtils.isEmpty(all)) {
            return Collections.emptyList();
        } else {
            return  transferMapper.loadAll(all);
        }
    }


    public void createTransaction(@RequestBody TransferDto transferDto)
            throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {

        Compte compteEmetteur = compteRepository.findByNrCompte(transferDto.getNrCompteEmetteur());
        Compte compteBeneficiere = compteRepository.findByNrCompte(transferDto.getNrCompteBeneficiaire());

        if (compteEmetteur == null) {
            logger.info("Compte Emetteur Non existant");
            throw new CompteNonExistantException(Constantes.COMPTE_E_NON_EXISTANT);
        }else
            if (compteBeneficiere == null) {
            logger.info("Compte Beneficiere Non existant");
            throw new CompteNonExistantException(Constantes.COMPTE_B_NON_EXISTANT);
        }

        BigDecimal montantTransfer = transferDto.getMontant();

        if (montantTransfer == null || montantTransfer.equals(BigDecimal.valueOf(0))) {
            logger.info("Montant vide");
            throw new TransactionException(Constantes.MONTANT_VIDE);

        }else if (montantTransfer.compareTo(Constantes.MONTANT_MINIMAL)<0) {
            logger.info("Montant minimal de transfer non atteint");
            throw new TransactionException(Constantes.MONTANT_OPERATION_MIN);

        } else if (montantTransfer.compareTo(Constantes.MONTANT_MAXIMAL)>0) {
            logger.info("Montant maximal de transfer dépassé");
            throw new TransactionException(Constantes.MONTANT_OPERATION_MAX);

        } else if(compteEmetteur.getSolde().subtract(montantTransfer).compareTo(new BigDecimal(0))<0){
            logger.error("Solde insuffisant pour l'utilisateur");
            throw new SoldeDisponibleInsuffisantException(Constantes.SOLDE_INSUFFISANT);
        }

        if (transferDto.getMotif().length() == 0) {
            logger.info("Motif vide");
            throw new TransactionException(Constantes.MOTIF_VIDE);
        }





        compteEmetteur.setSolde(compteEmetteur.getSolde().subtract(montantTransfer));
        compteRepository.save(compteEmetteur);

        compteBeneficiere.setSolde(compteBeneficiere.getSolde().add(montantTransfer));
        compteRepository.save(compteBeneficiere);

        Transfer transfer = new Transfer();
        transfer.setDateExecution(transferDto.getDate());
        transfer.setCompteBeneficiaire(compteBeneficiere);
        transfer.setCompteEmetteur(compteEmetteur);
        transfer.setMotifTransfer(transferDto.getMotif());
        transfer.setMontantTransfer(montantTransfer);

        save(transfer);

        auditService.auditTransfer("Transfer depuis " + transferDto.getNrCompteEmetteur() + " vers " + transferDto
                .getNrCompteBeneficiaire() + " d'un montant de " + transferDto.getMontant()
                .toString());

    }


    private void save(Transfer transfer) {
        transferRepository.save(transfer);
    }

}
