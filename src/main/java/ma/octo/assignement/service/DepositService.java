package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Deposit;

import ma.octo.assignement.dto.DepositDto;

import ma.octo.assignement.exceptions.CompteNonExistantException;

import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.DepositMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.DepositRepository;
import ma.octo.assignement.service.constantes.Constantes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestBody;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

@Service
@Transactional
public class DepositService {
    Logger logger = LoggerFactory.getLogger(DepositService.class);
    @Autowired
    private DepositRepository depositRepository;
    @Autowired
    private DepositMapper depositMapper;
    @Autowired
    private CompteRepository compteRepository;
    @Autowired
    private AuditService auditService;


    public List<DepositDto> loadAllDeposit() {
        logger.info("Lister des deposit");
        var all = depositRepository.findAll();

        if (CollectionUtils.isEmpty(all)) {
            return Collections.emptyList();
        } else {
            return  depositMapper.loadAll(all);
        }
    }

    public void createDeposit(@RequestBody DepositDto depositDto)
            throws  CompteNonExistantException, TransactionException {

        String nomPrenomEmetteur= depositDto.getNomPrenomEmetteur();
        Compte compteBeneficiere = compteRepository.findCompteByRib(depositDto.getRib());

        if (compteBeneficiere == null) {
            logger.info("Compte Beneficiere Non existant");
            throw new CompteNonExistantException(Constantes.COMPTE_B_NON_EXISTANT);
        }

        BigDecimal montantDepo = depositDto.getMontant();
        if (montantDepo == null || montantDepo.equals(BigDecimal.valueOf(0))) {
            logger.info("Montant vide");
            throw new TransactionException(Constantes.MONTANT_VIDE);

        } else if (montantDepo.compareTo(Constantes.MONTANT_MINIMAL)<0) {
            logger.info("Montant minimal de depot non atteint");
            throw new TransactionException(Constantes.MONTANT_OPERATION_MIN);

        } else if (depositDto.getMontant().compareTo(Constantes.MONTANT_MAXIMAL)>0) {
            logger.info("Montant maximal de depot dépassé");
            throw new TransactionException(Constantes.MONTANT_OPERATION_MAX);
        }

        if (depositDto.getMotif().length() < 0) {
            logger.info("Motif vide");
            throw new TransactionException(Constantes.MOTIF_VIDE);
        }

        compteBeneficiere.setSolde(compteBeneficiere.getSolde().add(montantDepo));
        compteRepository.save(compteBeneficiere);

        Deposit depo= new Deposit();
        depo.setMontant(montantDepo);
        depo.setDateExecution(depositDto.getDate());
        depo.setNomPrenomEmetteur(nomPrenomEmetteur);
        depo.setCompteBeneficiaire(compteBeneficiere);
        depo.setMotifDeposit(depositDto.getMotif());

        save(depo);

        auditService.auditDeposit("Depot par " + depositDto.getNomPrenomEmetteur() + " vers " +
                depositDto.getRib() + " d'un montant de " +
                depositDto.getMontant()
                .toString());
    }


    private void save(Deposit deposit) {
        depositRepository.save(deposit);
    }


}
