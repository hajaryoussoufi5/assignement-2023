package ma.octo.assignement.service;

import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.TransferMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.TransferRepository;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
class TransferServiceTest {
    @MockBean
    private AuditService auditService;

    @MockBean
    private CompteRepository compteRepository;

    @MockBean
    private TransferMapper transferMapper;

    @MockBean
    private TransferRepository transferRepository;

    @Autowired
    private TransferService transferService;

    @Test
    void testLoadAllTransfer() {

        when(transferRepository.findAll()).thenReturn(Collections.emptyList());
        assertTrue(transferService.loadAllTransfer().isEmpty());
        verify(transferRepository).findAll();
    }


    @Test
    void testCreateTransaction()
            throws CompteNonExistantException, SoldeDisponibleInsuffisantException, TransactionException {


        Utilisateur utilisateur1 = new Utilisateur();
        utilisateur1.setBirthdate(new Date());
        utilisateur1.setFirstname("hajar");
        utilisateur1.setLastname("Youssoufi");
        utilisateur1.setUsername("Youssoufi");
        utilisateur1.setGender("Gender");
        utilisateur1.setId(123L);

        Compte compte1 = new Compte();
        compte1.setId(123L);
        compte1.setNrCompte("123456789");
        compte1.setRib("Rib1");
        compte1.setSolde(BigDecimal.valueOf(42L));
        compte1.setUtilisateur(utilisateur1);

        Utilisateur utilisateur2 = new Utilisateur();
        utilisateur2.setBirthdate(new Date());
        utilisateur2.setFirstname("Hajar");
        utilisateur2.setLastname("youssoufi");
        utilisateur2.setUsername("youssoufi");
        utilisateur2.setGender("Gender");
        utilisateur2.setId(123L);


        Compte compte2 = new Compte();
        compte2.setId(123L);
        compte2.setNrCompte("23480668043");
        compte2.setRib("Rib2");
        compte2.setSolde(BigDecimal.valueOf(42L));
        compte2.setUtilisateur(utilisateur2);

        when(compteRepository.save(any())).thenReturn(compte1);
        when(compteRepository.findByNrCompte(any())).thenReturn(compte2);


        Utilisateur utilisateur3 = new Utilisateur();
        utilisateur3.setBirthdate(new Date());
        utilisateur3.setFirstname("first");
        utilisateur3.setGender("Gender");
        utilisateur3.setId(123L);
        utilisateur3.setLastname("last");
        utilisateur3.setUsername("username");

        Compte compte3 = new Compte();
        compte3.setId(123L);
        compte3.setNrCompte("Nr Compte");
        compte3.setRib("Rib");
        compte3.setSolde(BigDecimal.valueOf(42L));
        compte3.setUtilisateur(utilisateur3);


        Transfer transfer = new Transfer();
        transfer.setCompteBeneficiaire(compte1);
        transfer.setCompteEmetteur(compte2);
        transfer.setDateExecution(new Date());
        transfer.setId(123L);
        transfer.setMontantTransfer(BigDecimal.valueOf(42L));
        transfer.setMotifTransfer("Motif Transfer");

        when(transferRepository.save(any())).thenReturn(transfer);

        TransferDto transferDto = new TransferDto();
        transferDto.setDate(new Date());
        transferDto.setMontant(BigDecimal.valueOf(42L));
        transferDto.setMotif("Motif");
        transferDto.setNrCompteBeneficiaire("Nr Compte Beneficiaire");
        transferDto.setNrCompteEmetteur("Nr Compte Emetteur");

        transferService.createTransaction(transferDto);


        verify(compteRepository, atLeast(1)).save(any());
        verify(compteRepository, atLeast(1)).findByNrCompte(any());
        verify(transferRepository).save(any());
    }


}
