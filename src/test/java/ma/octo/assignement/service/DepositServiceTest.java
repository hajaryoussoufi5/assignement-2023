package ma.octo.assignement.service;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.DepositMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.DepositRepository;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
class DepositServiceTest {
    @MockBean
    private CompteRepository compteRepository;
    @MockBean
    private DepositRepository depositRepository;
    @MockBean
    private DepositMapper depositMapper;
    @MockBean
    private AuditService auditService;
    @Autowired
    private DepositService depositService;

    @Test
    void testLoadAllDeposit() {
        when(depositRepository.findAll()).thenReturn(Collections.emptyList());
        assertTrue(depositService.loadAllDeposit().isEmpty());
        verify(depositRepository).findAll();
    }


    @Test
    void testCreateDeposit() throws CompteNonExistantException, TransactionException {


        Utilisateur utilisateur1 = new Utilisateur();
        utilisateur1.setBirthdate(new Date());
        utilisateur1.setFirstname("hajar");
        utilisateur1.setLastname("Youssoufi");
        utilisateur1.setUsername("Youssoufi");
        utilisateur1.setGender("Gender");
        utilisateur1.setId(123L);


        Compte compte1 = new Compte();
        compte1.setId(123L);
        compte1.setNrCompte("123456789");
        compte1.setRib("Rib1");
        compte1.setSolde(BigDecimal.valueOf(42L));
        compte1.setUtilisateur(utilisateur1);

        Utilisateur utilisateur2 = new Utilisateur();
        utilisateur2.setBirthdate(new Date());
        utilisateur2.setFirstname("first");
        utilisateur2.setLastname("last");
        utilisateur2.setUsername("username");
        utilisateur2.setGender("Gender");
        utilisateur2.setId(123L);


        Compte compte2 = new Compte();
        compte2.setId(123L);
        compte2.setNrCompte("23480668043");
        compte2.setRib("Rib2");
        compte2.setSolde(BigDecimal.valueOf(42L));
        compte2.setUtilisateur(utilisateur2);

        when(compteRepository.save(any())).thenReturn(compte1);//

        when(compteRepository.findCompteByRib(any())).thenReturn(compte2);//

        Deposit deposit = new Deposit();
        deposit.setCompteBeneficiaire(compte1);
        deposit.setMotifDeposit("Motif Deposit");
        deposit.setNomPrenomEmetteur("Nom Prenom Emetteur");
        deposit.setDateExecution(new Date());
        deposit.setId(123L);
        deposit.setMontant(BigDecimal.valueOf(42L));


        when(depositRepository.save(any())).thenReturn(deposit);

        DepositDto depositDto = new DepositDto();
        depositDto.setDate(new Date());
        depositDto.setMontant(BigDecimal.valueOf(42L));
        depositDto.setMotif("Motif");
        depositDto.setNomPrenomEmetteur("Nom Prenom Emetteur");
        depositDto.setRib("Rib");


        depositService.createDeposit(depositDto);

        verify(compteRepository).save(any());
        verify(compteRepository).findCompteByRib(any());
        verify(depositRepository).save(any());
    }
}

