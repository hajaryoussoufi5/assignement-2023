package ma.octo.assignement.repository;


import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.domain.Utilisateur;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;


@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class TransferRepositoryTest {

    @Autowired
    private TransferRepository transferRepository;


    @Test
    public void findOne() {
    }


    @Test
    public void findAll() {
    }

    @Test
    public void save() {

        Utilisateur utilisateur1 = new Utilisateur();
        utilisateur1.setBirthdate(new Date());
        utilisateur1.setFirstname("hajar");
        utilisateur1.setLastname("Youssoufi");
        utilisateur1.setUsername("Youssoufi");
        utilisateur1.setGender("Gender");
        utilisateur1.setId(123L);

        Compte compte1 = new Compte();
        compte1.setId(123L);
        compte1.setNrCompte("123456789");
        compte1.setRib("Rib1");
        compte1.setSolde(BigDecimal.valueOf(42L));
        compte1.setUtilisateur(utilisateur1);

        Utilisateur utilisateur2 = new Utilisateur();
        utilisateur2.setBirthdate(new Date());
        utilisateur2.setFirstname("Hajar");
        utilisateur2.setLastname("youssoufi");
        utilisateur2.setUsername("youssoufi");
        utilisateur2.setGender("Gender");
        utilisateur2.setId(123L);


        Compte compte2 = new Compte();
        compte2.setId(123L);
        compte2.setNrCompte("23480668043");
        compte2.setRib("Rib2");
        compte2.setSolde(BigDecimal.valueOf(42L));
        compte2.setUtilisateur(utilisateur2);

        Transfer transfer = new Transfer();
        transfer.setCompteEmetteur(compte1);
        transfer.setCompteBeneficiaire(compte2);
        transfer.setMotifTransfer("Motif");
        transfer.setMontantTransfer(BigDecimal.valueOf(2435L));
        transfer.setDateExecution(new Date());

        Transfer testTransfer = transferRepository.save(transfer);

        assertThat(testTransfer).hasFieldOrPropertyWithValue("compteEmetteur", compte1);
        assertThat(testTransfer).hasFieldOrPropertyWithValue("compteBeneficiaire", compte2);


    }

}
