package ma.octo.assignement.repository;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.DepositRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.service.AuditService;
import ma.octo.assignement.service.DepositService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class DepositRepositoryTest {

    @Autowired
    private DepositRepository depositRepository;

    @Test
    public void findAll() {
    }

    @Test
    public void save(){

        Compte compte1 = new Compte();
        compte1.setNrCompte("010000A000001000");
        compte1.setRib("RIB1");
        compte1.setSolde(new BigDecimal(200000L));

        Deposit deposit = new Deposit();
        deposit.setCompteBeneficiaire(compte1);
        deposit.setNomPrenomEmetteur("Yousspifi hajar");
        deposit.setMotifDeposit("depot de 5000dh");
        deposit.setMontant(new BigDecimal(5000L));
        deposit.setDateExecution(new Date());


        Deposit depot = depositRepository.save(deposit);

        assertThat(depot).hasFieldOrPropertyWithValue("nomPrenomEmetteur", "Yousspifi hajar");
        assertThat(depot).hasFieldOrPropertyWithValue("compteBeneficiaire", compte1);
    }




}
